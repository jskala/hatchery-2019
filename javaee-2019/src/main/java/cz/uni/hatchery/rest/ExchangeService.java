package cz.uni.hatchery.rest;

import cz.uni.hatchery.beans.ExchangeBean;
import cz.uni.hatchery.dto.ExchangeDto;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Skala Jaroslav
 */
@Path("/exchange")
@SessionScoped
public class ExchangeService implements Serializable {

    private final static Logger LOGGER = Logger.getLogger(ExchangeService.class.getName());

    @Inject
    private ExchangeBean exchangeBean;

    @GET
    @Path("/load")
    @Produces(MediaType.TEXT_PLAIN)
    public String loadItem(@QueryParam("czkAmount") BigDecimal amount) {
        ExchangeDto exchangeCourses = exchangeBean.getExchangeCourses();
        LOGGER.log(Level.INFO, "Exchange rate for EUR is:" + exchangeCourses.getRates().get("CZK"));
        BigDecimal eur = exchangeCourses.getRates().get("CZK");
        if (eur == null) {
            LOGGER.log(Level.WARNING, "EUR is null.");
            return "";
        }
        return amount + " in CZK is " + amount.divide(eur, 4, BigDecimal.ROUND_DOWN) + " in EUR";
    }

}
