package cz.uni.hatchery.rest;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Skala Jaroslav
 */
@Provider
public class RestResponseFilter implements ContainerResponseFilter {
    private final static Logger LOGGER = Logger.getLogger(RestResponseFilter.class.getName());

    @Override public void filter(ContainerRequestContext containerRequestContext, ContainerResponseContext containerResponseContext) throws IOException {
        LOGGER.log(Level.INFO,"Response is: "+containerResponseContext.getEntity().toString());
    }
}
