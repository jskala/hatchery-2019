package cz.uni.hatchery.rest;

import cz.uni.hatchery.dao.CityDao;
import cz.uni.hatchery.dao.UserDao;
import cz.uni.hatchery.dto.CityDto;
import cz.uni.hatchery.dto.UserDto;
import cz.uni.hatchery.entities.City;
import cz.uni.hatchery.entities.User;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Skala Jaroslav
 */
@SessionScoped
@Path("/user")
public class UserService implements Serializable {

    @Inject
    private UserDao userDao;

    @Inject
    private CityDao cityDao;

    @PUT
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public void save(UserDto user) {
        CityDto cityDto = user.getCity();

        User u = new User(user.getFirstName(), user.getLastName());
        City city = new City(cityDto.getName(), cityDto.getNumberOfCitizens(),
                Arrays.asList(u));
        u.setCity(city);
        cityDao.save(city);
//        userDao.save(u);
    }

    @GET
    @Path("/load/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UserDto load(@PathParam("id") Integer id) {
        User user = userDao.get(id);
        UserDto userDto = new UserDto(user.getId(),
                user.getFirsName(), user.getLastName());
        return userDto;
    }

    @GET
    @Path("/load/all")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UserDto> load() {
        List<User> all = userDao.getAll();

        List<UserDto> resultList = new ArrayList<>();
        for (int i = 0; i < all.size(); i++) {
            User user = all.get(i);
            UserDto userDto = new UserDto(user.getId(),
                    user.getFirsName(), user.getLastName());

            City city = user.getCity();
            CityDto cityDto = new CityDto(city.getId(),
                    city.getName(),city.getNumberOfCitizens());
            userDto.setCity(cityDto);
            resultList.add(userDto);
        }
//        return resultList;

        return all.stream()
                .map(i->new UserDto(i.getId(), i.getFirsName(), i.getLastName(),
                        new CityDto(i.getCity().getId(),i.getCity().getName(),
                                i.getCity().getNumberOfCitizens())))
                .collect(Collectors.toList());
    }

}