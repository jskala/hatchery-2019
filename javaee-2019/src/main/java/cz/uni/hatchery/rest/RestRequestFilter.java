package cz.uni.hatchery.rest;


import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * @author Skala Jaroslav
 */
@Provider
public class RestRequestFilter implements ContainerRequestFilter {
    private final static Logger LOGGER = Logger.getLogger(RestRequestFilter.class.getName());



    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        LOGGER.log(Level.INFO,"Request is: "+containerRequestContext.getRequest());
    }
}
