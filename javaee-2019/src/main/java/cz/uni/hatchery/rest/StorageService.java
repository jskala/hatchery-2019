package cz.uni.hatchery.rest;

import cz.uni.hatchery.beans.ProductStorage;
import cz.uni.hatchery.dto.Product;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import java.io.Serializable;

/**
 * @author Skala Jaroslav
 */
@Path("/storage")
@SessionScoped
public class StorageService implements Serializable {

    @Inject
    private ProductStorage productStorage;

    @GET
    @Path("/load")
    @Produces(MediaType.APPLICATION_JSON)
    public Product loadItem(@QueryParam("id") String id) {
        Product load = productStorage.load(id);
        if(load == null){
            return null;
        }
        return load;
    }


    @PUT
    @Path("/save")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Product storeItem(Product product){
        productStorage.save(product);
        return product;
    }
}
