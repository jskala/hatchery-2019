package cz.uni.hatchery.beans;

import cz.uni.hatchery.dto.Product;

import javax.ejb.Local;
import javax.ejb.Stateful;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Skala Jaroslav
 */
//@SessionScoped
@Stateful
@Local
public class ProductStorage implements Serializable {

    private Map<String, Product> productMap;

    public Map<String, Product> getAllProducts() {
        return productMap;
    }

    public Product load(String id) {
        if (productMap == null) {
            productMap = new HashMap<>();
        }
        return productMap.get(id);
    }

    public Product save(Product product) {
        if (productMap == null) {
            productMap = new HashMap<>();
        }
        Product put = productMap.put(product.getId(), product);
        return put;

    }

}
