package cz.uni.hatchery.beans;

import javax.ejb.Local;
import javax.ejb.Stateful;

/**
 * @author Skala Jaroslav
 */

//<dependency>
//<groupId>javax.ejb</groupId>
//<artifactId>ejb-api</artifactId>
//<version>3.0</version>
//</dependency>
@Stateful
@Local
public class CounterBean {

    private int counter;

    public void increase(int n) {
        counter = counter + n;
    }

    public int getCounter() {
        return counter;
    }
}
