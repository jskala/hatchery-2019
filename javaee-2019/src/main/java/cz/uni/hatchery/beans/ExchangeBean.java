package cz.uni.hatchery.beans;

import cz.uni.hatchery.dto.ExchangeDto;

import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import java.io.Serializable;

/**
 * @author Skala Jaroslav
 */
@Stateless
public class ExchangeBean implements Serializable {

//            <dependency>
//            <groupId>javax.ws.rs</groupId>
//            <artifactId>javax.ws.rs-api</artifactId>
//            <version>2.1.1</version>
//        </dependency>

    public ExchangeDto getExchangeCourses(){
        Client client = ClientBuilder.newClient();
        ExchangeDto result = client.target("https://api.exchangeratesapi.io/latest")
                .request(MediaType.APPLICATION_JSON)
                .get(ExchangeDto.class);

        return result;
    }

}
