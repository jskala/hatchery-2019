package cz.uni.hatchery.dto;

import javax.xml.bind.annotation.XmlRootElement;

import java.io.Serializable;

/**
 * @author Skala Jaroslav
 */
@XmlRootElement
public class Product implements Serializable {

    private String id;
    private String name;
    private Integer size;

    public Product(String id, String name, Integer size) {
        this.id = id;
        this.name = name;
        this.size = size;
    }

    public Product() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    @Override public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", size=" + size +
                '}';
    }
}
