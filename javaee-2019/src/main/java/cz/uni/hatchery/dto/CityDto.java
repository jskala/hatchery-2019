package cz.uni.hatchery.dto;

/**
 * @author Skala Jaroslav
 */
public class CityDto {

    private Integer id;

    private String name;

    private Integer numberOfCitizens;

    public CityDto() {
    }

    public CityDto(Integer id, String name, Integer numberOfCitizens) {
        this.id = id;
        this.name = name;
        this.numberOfCitizens = numberOfCitizens;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumberOfCitizens() {
        return numberOfCitizens;
    }

    public void setNumberOfCitizens(Integer numberOfCitizens) {
        this.numberOfCitizens = numberOfCitizens;
    }
}
