package cz.uni.hatchery.dto;

import java.math.BigDecimal;
import java.util.Map;

/**
 * @author Skala Jaroslav
 */
public class ExchangeDto {

    private String base;
    private String date;
    private Map<String, BigDecimal> rates;

    public ExchangeDto(String base, String date, Map<String, BigDecimal> rates) {
        this.base = base;
        this.date = date;
        this.rates = rates;
    }

    public ExchangeDto() {
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Map<String, BigDecimal> getRates() {
        return rates;
    }

    public void setRates(Map<String, BigDecimal> rates) {
        this.rates = rates;
    }

    @Override public String toString() {
        return "ExchangeDto{" +
                "base='" + base + '\'' +
                ", date=" + date +
                ", rates=" + rates +
                '}';
    }
}
