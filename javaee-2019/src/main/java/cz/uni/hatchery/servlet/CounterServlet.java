package cz.uni.hatchery.servlet;

import cz.uni.hatchery.beans.CounterBean;

import javax.inject.Inject;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Skala Jaroslav
 */
public class CounterServlet extends HttpServlet {


    @Inject
    private CounterBean counterBean;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        String input = req.getParameter("number");
        counterBean.increase(Integer.parseInt(input));
//        counterBean.increase(1);

        PrintWriter writer = resp.getWriter();
        writer.println("<html>");
        writer.println("<body>");

        writer.println("<h1>Actual count is:"
                + counterBean.getCounter() + "</h1>");

        writer.println("</body>");
        writer.println("</html>");

    }

}