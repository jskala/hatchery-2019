package cz.uni.hatchery.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Skala Jaroslav
 */
public class HelloWorldServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        String input = req.getParameter("input");

        PrintWriter writer = resp.getWriter();
        writer.println("<html>");
        writer.println("<body>");

        writer.println("<h1>Hello World! I am doGet Servlet. Input is: "
                + input + "</h1>");

        writer.println("</body>");
        writer.println("</html>");

    }

}
