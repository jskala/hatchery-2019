package cz.uni.hatchery.test;

/**
 * @author Skala Jaroslav
 */
public class Calculater {

    public int add(int a, int b){
        return a+b;
    }

    public int multiply(int a, int b){
        return Math.multiplyExact(a, b);
    }
}
