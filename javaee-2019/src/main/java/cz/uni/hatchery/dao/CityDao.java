package cz.uni.hatchery.dao;

import cz.uni.hatchery.entities.City;

import javax.enterprise.context.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import java.io.Serializable;

/**
 * @author Skala Jaroslav
 */
@SessionScoped
public class CityDao implements Serializable {

    @PersistenceContext(unitName = "test")
    private EntityManager entityManager;


    @Transactional
    public void save(City city){
        entityManager.persist(city);
    }



}
