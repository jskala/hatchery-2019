package cz.uni.hatchery.dao;

import cz.uni.hatchery.entities.User;

import javax.enterprise.context.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * @author Skala Jaroslav
 */
@SessionScoped
public class UserDao implements Serializable {


    @PersistenceContext(unitName = "test")
    private EntityManager entityManager;


    @Transactional
    public void save(User user){
        entityManager.persist(user);
    }

    public void update(User user){
        entityManager.merge(user);
    }

    public User get(Integer id){
        return entityManager.find(User.class, id);
    }

    public List<User> getAll(){
        return entityManager.
                createNamedQuery("User.getAll", User.class)
                .getResultList();
    }
}
