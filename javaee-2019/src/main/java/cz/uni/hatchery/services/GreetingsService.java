package cz.uni.hatchery.services;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

/**
 * @author Skala Jaroslav
 */
@WebService
public class GreetingsService {

    private final String message = "Hello, ";

    public GreetingsService() {
    }

    @WebMethod
    @WebResult(name = "greetings")
    public String sayHello(@WebParam(name = "name") String name) {
        return message + name + ".";
    }

}
