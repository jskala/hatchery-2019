package cz.uni.hatchery.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Skala Jaroslav
 */

@Entity
@Table(name = "CITY")
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "NUMBEROFCITYZENS")
    private Integer numberOfCitizens;

    @OneToMany(
            mappedBy = "city",
            cascade = CascadeType.ALL
    )
    private List<User> users = new ArrayList<>();

    public City() {
    }

    public City(String name, Integer numberOfCitizens, List<User> users) {
        this.name = name;
        this.numberOfCitizens = numberOfCitizens;
        this.users = users;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumberOfCitizens() {
        return numberOfCitizens;
    }

    public void setNumberOfCitizens(Integer numberOfCitizens) {
        this.numberOfCitizens = numberOfCitizens;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
