package cz.uni.hatchery;

import cz.uni.hatchery.rest.ExchangeService;
import cz.uni.hatchery.rest.StorageService;
import cz.uni.hatchery.rest.UserService;

import javax.ws.rs.core.Application;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Skala Jaroslav
 */
public class MyApplication extends Application {

    public Set<Class<?>> getClasses()
    {
        Set<Class<?>> s = new HashSet<Class<?>>();
        s.add(StorageService.class);
        s.add(ExchangeService.class);
        s.add(UserService.class);
        return s;
    }

}
